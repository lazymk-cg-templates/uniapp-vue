/**
 * 示例模块
 * @author Caesar Liu
 * @date 2021-04-14 21:13
 */
const state = {
  sampleStateContent: 'Hello (From Module State)'
}
const mutations = {}
const actions = {}
const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}