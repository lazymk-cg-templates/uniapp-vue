const config = require('./config')
const VARS = config
// 开发环境
if (config.ENV === 'development') {
	Object.assign(VARS, require('./.env.dev'));
}
// 测试环境
if (config.ENV === 'testing') {
	Object.assign(VARS, require('./.env.testing'));
}
// 生产环境
if (config.ENV === 'production') {
	Object.assign(VARS, require('./.env.prod'));
}
export default VARS
