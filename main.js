import Vue from 'vue'
import App from './App'
import Store from './store'

Vue.config.productionTip = false
Vue.prototype.$baseUrl="http://localhost:8081" 

App.mpType = 'app'
App.store = Store

const app = new Vue({
    ...App
})
app.$mount()
